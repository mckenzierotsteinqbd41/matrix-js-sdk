# `im`

## 如何开发

### 1. 运行 Synapse home server

##### 1.1 安装 docker

windows 或者 mac 系统安装 [Docker Desktop](https://docs.docker.com/desktop/install/mac-install/) 即可.

##### 1.2 运行 synapse compose

如果是用的 windows 机器，将 [./server/docker-compose.yml](./server/docker-compose.yml) 复制到 wsl2 中(注意要 mkdir 一个目录，因为 files 和 schemas volume 会保存其中)，然后运行后续命令

运行 :
```sh
docker-compose run --rm -e SYNAPSE_SERVER_NAME=my.matrix.host -e SYNAPSE_REPORT_STATS=yes synapse generate
```

以生成必要的签名.

由于现有的权限问题, 需要在生成后更改目录权限, 参考 [这个issue](https://github.com/element-hq/synapse/issues/16824) 来修改生成的权限。

完成后运行 `docker-compose up -d`

然后查看日志, 如果没有出现这样的 error :
```sh
Error in configuration at 'signing_key':
  Error accessing file '/data/my.matrix.host.signing.key':
    [Errno 13] Permission denied: '/data/my.matrix.host.signing.key'
```
说明运行成功

##### 1.3 创建 user 以供开发

首先根据 [这个文档](https://github.com/element-hq/synapse?tab=readme-ov-file#registering-a-new-user-from-a-client) 修改配置，以运行从 client 创建用户，修改好后重新启动镜像。

然后根据这个 [create account api](https://element-hq.github.io/synapse/latest/admin_api/user_admin_api.html#create-or-modify-account) 来创建用户。

接着获取 [access token](https://element-hq.github.io/synapse/latest/admin_api/user_admin_api.html#login-as-a-user)

根据以上获取的内容，在客户端进行开发

### 2. matrix server 的后续迭代

后续对账户和 event 等的修改如果存在, 那么修改当前 repo 的代码，
并将该 repo 修改为 monorepo 结构，以方便的进行开发 fork 后的版本.
