import { IIdentityProvider, createClient } from 'matrix-js-sdk';
import React, { useMemo } from 'react';

type SSOLoginProps = {
  providers: IIdentityProvider[];
  asIcons?: boolean;
  redirectUrl: string;
};
export function SSOLogin({ providers, redirectUrl, asIcons }: SSOLoginProps) {
  const baseUrl = 'https://matrix.org';
  const mx = useMemo(() => createClient({ baseUrl }), [baseUrl]);

  const getSSOIdUrl = (ssoId: string): string => mx.getSsoLoginUrl(redirectUrl, 'sso', ssoId);

  return (
    <div>
      {providers.map((provider) => {
        const { id, name, icon } = provider;
        const iconUrl = icon && mx.mxcUrlToHttp(icon, 96, 96, 'crop', false);

        const buttonTitle = `Continue with ${name}`;

        if (iconUrl && asIcons) {
          return (
            <a
              style={{ cursor: 'pointer' }}
              key={id}
              href={getSSOIdUrl(id)}
              aria-label={buttonTitle}
            >
              <img src={iconUrl} alt={name} title={buttonTitle} />
            </a>
          );
        }

        return (
          <a
            style={{ width: '100%' }}
            key={id}
            href={getSSOIdUrl(id)}
          >
            <span>
              {buttonTitle}
            </span>
          </a>
        );
      })}
    </div>
  );
}
