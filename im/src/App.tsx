import React from 'react';
import { Provider as JotaiProvider } from 'jotai';
import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createHashRouter,
  createRoutesFromElements,
  redirect,
} from 'react-router-dom';

import { LOGIN_PATH, REGISTER_PATH, RESET_PASSWORD_PATH, ROOT_PATH } from './paths';
import { getLoginPath } from './pathUtils';

const createRouter = () => {
    const isAuthenticated = () => true;

  const routes = createRoutesFromElements(
    <Route>
      <Route
        path={ROOT_PATH}
        loader={() => {
          if (isAuthenticated()) return redirect('/home');
          return redirect(getLoginPath());
        }}
      />

      <Route
        loader={() => {
          if (!isAuthenticated()) return redirect(getLoginPath());
          return null;
        }}
      >
        <Route path="/home" element={<></>} />
        <Route path="/direct" element={<p>direct</p>} />
        <Route path="/:spaceIdOrAlias" element={<p>:spaceIdOrAlias</p>} />
        <Route path="/explore" element={<p>explore</p>} />
      </Route>
      <Route path="/*" element={<p>Page not found</p>} />
    </Route>
  );

    return createHashRouter(routes, { basename: '/' });

};

function App() {
  return (
            <JotaiProvider>
              <RouterProvider router={createRouter()} />
            </JotaiProvider>
  );
}

export {
    App
}
